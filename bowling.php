<?php


class Bowling {

    private $rolls = array();

    public function roll($pins) {
        $this->rolls[] = $pins;
    }

    public function stringToPins($stringFrame){
        $arryFrame = str_split($stringFrame);
        $max=sizeof($arryFrame);
        for($pins=0; $pins<$max; $pins++) {

            switch ($arryFrame[$pins]) {
                case "x":
                    $this->roll(10);
                    break;
                case "/":
                    $this->roll(5); $this->rolls(5);
                    break;
                case "-":
                    $this->roll(0);
                    break;
                default:
                    $this->roll((int)$arryFrame[$pins]);
                    break;
            }
        }
    }

    public function isStrike($counterFrame) {
        return $this->rolls[$counterFrame] == 10 ? true : false;
    }

    public function isSpare($counterFrame) {
        return $this->rolls[$counterFrame] + $this->rolls[$counterFrame+1]  == 10 ? true : false;
    }

    public function nextTwoBallsForStrike($counterFrame) {
        return $this->rolls[$counterFrame + 1] + $this->rolls[$counterFrame + 2];
    }

    public function nextBallForSpare($counterFrame) {
        return $this->rolls[$counterFrame + 2];
    }

    public function twoBallsInFrame($counterFrame) {
        return $this->rolls[$counterFrame] + $this->rolls[$counterFrame + 1];
    }


    public function score(){
        $counterFrame=0;
        $score = 0;

        for($frames=0; $frames<10; $frames++){

            if($this->isStrike($counterFrame))
            {
                $score += 10 + $this->nextTwoBallsForStrike($counterFrame);
                $counterFrame+=1;

            }else{
                if($this->isSpare($counterFrame))
                {
                    $score += 10 + $this->nextBallForSpare($counterFrame);
                    $counterFrame += 2;
                } else {
                    $score += $this->twoBallsInFrame($counterFrame);
                    $counterFrame += 2;
                }
            }
        }
        return $score;
    }


}





