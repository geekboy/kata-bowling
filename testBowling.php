<?php

require_once 'bowling.php';

class TestBowling extends PHPUnit_Framework_TestCase
{

    protected $game;

    protected function setUp()
    {
        $this->game = new Bowling();
    }

    public function moreBalls($n, $pins)
    {
        for ($i = 0; $i < $n; $i++) {
            $this->game->roll($pins);
        }
    }


    public function testScore0()
    {
        $this->moreBalls(20, 0);
        $this->assertEquals(0, $this->game->score());
    }

    public function testScore1()
    {
        $this->moreBalls(20, 1);
        $this->assertEquals(20, $this->game->score());
    }

    public function testScore2()
    {
        $this->moreBalls(20, 2);
        $this->assertEquals(40, $this->game->score());
    }

    public function testSpare()
    {
        $this->game->roll(7);
        $this->game->roll(3);
        $this->game->roll(3);
        $this->moreBalls(17, 0);
        $this->assertEquals(16, $this->game->score());
    }

    public function testSpare2()
    {
        $this->game->roll(8);
        $this->game->roll(2);
        $this->game->roll(3);
        $this->moreBalls(17, 1);
        $this->assertEquals(33, $this->game->score());
    }

    public function testStrike()
    {
        $this->game->roll(10);
        $this->game->roll(3);
        $this->game->roll(4);
        $this->moreBalls(16, 0);
        $this->assertEquals(24, $this->game->score());
    }

    public function testStrike2()
    {
        $this->game->roll(10);
        $this->game->roll(4);
        $this->game->roll(4);
        $this->moreBalls(16, 0);
        $this->assertEquals(26, $this->game->score());
    }

    public function testAllStrikes() {
        $this->moreBalls(12, 10);
        $this->assertEquals(300, $this->game->score());
    }

    public function testAllSpares() {
        $this->moreBalls(21, 5);
        $this->assertEquals(150, $this->game->score());
    }


    public function test9andMiss() {
        $this->moreBalls(10, 9);
        $this->moreBalls(10, 0);
        $this->assertEquals(90, $this->game->score());
    }

    public function testString() {
        $this->game->stringToPins("xxxxxxxxxxxx");
        $this->assertEquals(300, $this->game->score());
    }

    public function testString2() {
        $this->game->stringToPins("9-9-9-9-9-9-9-9-9-9-");
        $this->assertEquals(90, $this->game->score());
    }
}
